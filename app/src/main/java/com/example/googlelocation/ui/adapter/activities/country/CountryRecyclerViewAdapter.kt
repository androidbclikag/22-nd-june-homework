package com.example.googlelocation.ui.adapter.activities.country

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.googlelocation.R
import kotlinx.android.synthetic.main.country_item.view.*

class CountryRecyclerViewAdapter(private val countries: MutableList<CountryModel.ResultsCountry>) :
    RecyclerView.Adapter<CountryRecyclerViewAdapter.ViewHolder>() {
    var selectedCountry = 0

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        private lateinit var model: CountryModel.ResultsCountry
        fun onBind() {
            model = countries[adapterPosition]
            itemView.countryName.text = model.name
            itemView.setOnClickListener(this)
            itemView.checkedCountry.visibility = if (selectedCountry == adapterPosition)
                View.VISIBLE
            else
                View.INVISIBLE

        }


        override fun onClick(v: View?) {
            selectedCountry = adapterPosition
            notifyDataSetChanged()

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.country_item, parent, false
        )
    )

    override fun getItemCount(): Int = countries.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

}