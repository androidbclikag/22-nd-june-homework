package com.example.googlelocation.data

import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap


object CountryLoader {



    const val COUNTRY = "90aa5377-649b-4e39-ab7c-fa6a21a6882d"

    private var retrofit = Retrofit.Builder()
        .baseUrl("https://run.mocky.io/v3/")
        .addConverterFactory(ScalarsConverterFactory.create())
        .build()

    private var service = retrofit.create(
        ApiRequest::class.java)


    fun getRequest(path: String, parameters: MutableMap<String, String>, callBack: CustomCallBack) {
        val call = service.getRequest(path, parameters)
        call!!.enqueue(
            onCallBack(
                callBack
            )
        )


    }


    private fun onCallBack(callBack: CustomCallBack) = object : Callback<String> {
        override fun onFailure(call: retrofit2.Call<String>, t: Throwable) {
            callBack.onError(t.message.toString())
        }

        override fun onResponse(call: retrofit2.Call<String>, response: Response<String>) {
            callBack.onResponse(response.body().toString())
        }

    }


    interface ApiRequest {
        @GET("{path}")
        fun getRequest(
            @Path("path") path: String?,
            @QueryMap parameters: MutableMap<String, String>
        ): retrofit2.Call<String>?
    }
}