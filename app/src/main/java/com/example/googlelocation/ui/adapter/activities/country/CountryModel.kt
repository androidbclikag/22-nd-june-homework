package com.example.googlelocation.ui.adapter.activities.country

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CountryModel(val results:MutableList<ResultsCountry>): Parcelable {

    @Parcelize
    data class ResultsCountry(val name:String, val iso2:String): Parcelable

}