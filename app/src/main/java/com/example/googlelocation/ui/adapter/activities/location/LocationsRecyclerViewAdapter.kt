package com.example.googlelocation.ui.adapter.activities.location

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.googlelocation.R
import kotlinx.android.synthetic.main.location_item_layout.view.*

class LocationsRecyclerViewAdapter(
    private val locations: MutableList<LocationsModel>, private val itemClick:CustomOnClick

) :
    RecyclerView.Adapter<LocationsRecyclerViewAdapter.ViewHolder>() {
    var selectedPosition = 0
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        fun onBind() {
            val locModel = locations[adapterPosition]
            itemView.locationTextView.text = locModel.description
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            itemClick.onClick()
            selectedPosition = adapterPosition
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.location_item_layout, parent, false)
    )

    override fun getItemCount(): Int = locations.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }
}