package com.example.googlelocation.ui.adapter.activities.address

import com.google.gson.annotations.SerializedName

class ListingAddressComponents {
    @SerializedName("long_name")
    var longName = ""
    @SerializedName("short_name")
    var shortName = ""
    var types = ArrayList<String>()
}