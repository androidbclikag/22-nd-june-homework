package com.example.googlelocation.ui.adapter.activities.address

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.util.Log.d
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.example.googlelocation.R
import com.example.googlelocation.Tools
import com.example.googlelocation.data.CountryLoader
import com.example.googlelocation.data.CustomCallBack
import com.example.googlelocation.data.FutureCallBack
import com.example.googlelocation.data.LocationLoader
import com.example.googlelocation.ui.adapter.activities.country.CountryActivity
import com.example.googlelocation.ui.adapter.activities.country.CountryModel
import com.example.googlelocation.ui.adapter.activities.location.SearchLocationActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_save_address.*
import kotlinx.android.synthetic.main.city_text_field.*
import kotlinx.android.synthetic.main.country_text_field.*
import kotlinx.android.synthetic.main.state_text_field.*
import kotlinx.android.synthetic.main.street_address_text_field.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.zip_text_field.*
import java.util.ArrayList

class SaveAddressActivity : AppCompatActivity(), View.OnClickListener {
    private val countries = mutableListOf<CountryModel.ResultsCountry>()
    private var countryId = ""

    companion object {
        const val CHOOSE_COUNTRY = 20
        const val CHOOSE_STREET_ADDRESS = 21
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_save_address)

        init()
    }


    private fun init() {
        countryNameField.setOnClickListener(this)
        streetAddressET.setOnClickListener(this)
        attachToolBar()
        getCountry()
    }


    private fun attachToolBar() {
        setSupportActionBar(toolBar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        doneButton.text = resources.getString(R.string.finalize)
        titleTV.text = resources.getString(R.string.Address)
        doneButton.setOnClickListener {
            Toast.makeText(this, "Task is not completed yet", Toast.LENGTH_SHORT).show()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            super.onBackPressed()
            return true
        }

        return super.onOptionsItemSelected(item)
    }


    fun next(view: View) {
        Tools.initDialog(
            this,
            getString(R.string.error),
            getString(R.string.error_description)
        )


    }


    private fun selectCountry() {

        val intent = Intent(this, CountryActivity::class.java)
        intent.putParcelableArrayListExtra("countries", countries as ArrayList<out Parcelable>)
        (0 until countries.size).forEach {
            d("countries", countries[it].toString())
        }
        intent.putExtra("countryID", countryId)

        startActivityForResult(intent, CHOOSE_COUNTRY)
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out)

    }

    private fun getCountry() {


        CountryLoader.getRequest(CountryLoader.COUNTRY, mutableMapOf(), object : CustomCallBack {
            override fun onResponse(body: String) {
                countries.addAll(Gson().fromJson(body, CountryModel::class.java).results)

                if (countries.isNotEmpty())
                    setCountry(countries[0].name, countries[0].iso2)


            }

            override fun onError(throwable: String) {

            }
        })

    }

    private fun setCountry(countryName: String, countryId: String) {

        countryNameField.setText(countryName)

        this.countryId = countryId

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CHOOSE_COUNTRY) {
                val modelName = data!!.extras!!.getString("countryName")
                val modelId = data.extras!!.getString("countryID")

                d("countryName", modelName.toString())

                setCountry(modelName.toString(), modelId.toString())
            } else if (requestCode == CHOOSE_STREET_ADDRESS) {
                d("place_id", data!!.extras!!.getString("place_id", ""))
                d("desqription", data.extras!!.getString("description", ""))
                getAddress(
                    data!!.extras!!.getString("place_id", ""),
                    data.extras!!.getString("description", "")
                )
            }


        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.countryNameField -> selectCountry()

            R.id.streetAddressET -> chooseStreet()
        }
    }

    private fun chooseStreet() {

        val intent = Intent(this, SearchLocationActivity::class.java)
        if (cityET.text.isNullOrEmpty() || stateET.text.isNullOrEmpty() || zipCodeET.text.isNullOrEmpty()) {
            cityET.text!!.clear()
            stateET.text!! .clear()
            zipCodeET.text!! .clear()
        }
        intent.putExtra("countryID", countryId)
        startActivityForResult(intent, CHOOSE_STREET_ADDRESS)
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out)


    }


    private fun getAddress(placeID: String, streetAddress: String) {
        d("placeId", placeID)

        if (placeID.isNotEmpty()) {
            streetAddressET.setText(streetAddress)
            LocationLoader.getAddressDetail(placeID, object :
                FutureCallBack<MutableMap<String, String>> {

                override fun doneResults(body: HashMap<String, String>) {
                    if (body.containsKey("city"))
                        cityET.setText(body["city"])
                    if (body.containsKey("state"))
                        stateET.setText(body["state"])
                    if (body.containsKey("zipcode"))
                        zipCodeET.setText(body["zipcode"])

                }

            })
        }
    }


}
