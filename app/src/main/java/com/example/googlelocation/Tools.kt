package com.example.googlelocation

import android.app.Dialog
import android.content.Context
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import kotlinx.android.synthetic.main.dialog_layout.*

object Tools {
    fun initDialog(context: Context, title:String, description: String){
        val dialog = Dialog(context)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_layout)
        val parameters:ViewGroup.LayoutParams = dialog.window!!.attributes
        parameters.width = ViewGroup.LayoutParams.MATCH_PARENT
        parameters.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog.window!!.attributes = parameters as WindowManager.LayoutParams
        dialog.descriptionTV.text = description
        dialog.dialogTitleTV.text = title
        dialog.tryAgainBT.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()

    }



}