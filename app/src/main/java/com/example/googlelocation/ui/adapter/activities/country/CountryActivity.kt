package com.example.googlelocation.ui.adapter.activities.country

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.googlelocation.R
import kotlinx.android.synthetic.main.activity_country.*
import kotlinx.android.synthetic.main.toolbar.*

class CountryActivity : AppCompatActivity() {
    private val countries = mutableListOf<CountryModel.ResultsCountry>()
    private var countryId = ""
    private lateinit var countryRecyclerViewAdapter: CountryRecyclerViewAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_country)
        attachToolbar()
        init()
    }

    private fun init() {
        countryRecyclerView.layoutManager = LinearLayoutManager(this)
        countryRecyclerViewAdapter = CountryRecyclerViewAdapter(countries)
        countryRecyclerView.adapter = countryRecyclerViewAdapter
        countryRecyclerView.setHasFixedSize(true)
        getCountries()
    }


    private fun finalize() {
        val countryModel = countries[countryRecyclerViewAdapter.selectedCountry]
        d("logCountryModel", countryModel.name)
        val intent = intent
        intent.putExtra("countryName", countryModel.name)
        intent.putExtra("countryID", countryModel.iso2)
        setResult(Activity.RESULT_OK, intent)

        finish()

    }

    private fun getCountries() {
        countries.addAll(intent.extras!!.getParcelableArrayList("countries")!!)
        countryRecyclerViewAdapter.notifyDataSetChanged()

        countryId = intent!!.extras!!.getString("countryID").toString()
        (0 until countries.size).forEach {
            if (countryId == countries[it].iso2) {
                countryRecyclerViewAdapter.selectedCountry = it
//                d("countryModel", countries[it].toString())
            }

        }
    }


    private fun attachToolbar() {
        setSupportActionBar(toolBar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        doneButton.text = resources.getString(R.string.finalize)
        titleTV.text = resources.getString(R.string.country)
        doneButton.setOnClickListener {
            finalize()
        }
    }

    fun uncheckCountry(view: View) {

        backToAddressActivity()
    }

    private fun backToAddressActivity() {
        super.onBackPressed()
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out)

    }

    override fun onBackPressed() {
        backToAddressActivity()
    }

}
