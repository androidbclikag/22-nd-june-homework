package com.example.googlelocation.data

import android.util.Log.d
import com.example.googlelocation.App
import com.example.googlelocation.ui.adapter.activities.address.ListingAddressComponents
import com.google.gson.Gson
import org.json.JSONObject
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap


object LocationLoader {


    const val AUTOCOMPLETE = "autocomplete"
    private const val DETAILS = "details"

    private var retrofit = Retrofit.Builder()
        .baseUrl("https://maps.googleapis.com/maps/api/")
        .addConverterFactory(ScalarsConverterFactory.create())
        .build()

    private var service = retrofit.create(
        ApiRequest::class.java
    )


    fun getRequest(path: String, parameters: MutableMap<String, String>, callBack: CustomCallBack) {
        val call = service.getRequest(path, parameters)
        call!!.enqueue(
            onCallBack(
                callBack
            )
        )


    }


    private fun onCallBack(callBack: CustomCallBack) = object : Callback<String> {
        override fun onFailure(call: retrofit2.Call<String>, t: Throwable) {
            callBack.onError(t.message.toString())
        }

        override fun onResponse(call: retrofit2.Call<String>, response: Response<String>) {
            callBack.onResponse(response.body().toString())
        }

    }





    fun getAddressDetail(
        placeID: String,
        callback: FutureCallBack<MutableMap<String, String>>
    ) {
        d("place", placeID)
        val parameter: HashMap<String, String> = HashMap()
        parameter["placeid"] = placeID
        parameter["key"] = App.instance!!.getApiKey()
        getRequest(DETAILS, parameter, object : CustomCallBack {
            override fun onResponse(body: String) {
                val resultJson = JSONObject(body)
                d("getAddressDetail ", body)
                if (resultJson.has("status")) {
                    if (resultJson.getString("status") == "OK")
                        if (resultJson.has("result"))
                            if (resultJson.getJSONObject("result").has("address_components")) {
                                val listingAddressComponents: List<ListingAddressComponents>? =
                                    Gson().fromJson(
                                        resultJson.getJSONObject("result")
                                            .getJSONArray("address_components").toString(),
                                        Array<ListingAddressComponents>::class.java
                                    ).toList()
                                var lat = 0.0
                                var lng = 0.0
                                if (resultJson.getJSONObject("result").has("geometry")) {
                                    val location =
                                        resultJson.getJSONObject("result").getJSONObject("geometry")
                                            .getJSONObject("location")
                                    lat = location.getDouble("lat")
                                    lng = location.getDouble("lng")
                                }
                                val resultsMap = HashMap<String, String>()
                                resultsMap["lat"] = lat.toString()
                                resultsMap["lng"] = lng.toString()
                                for (item in listingAddressComponents!!) {
                                    when {
                                        item.types.contains("street_number") -> resultsMap["streetNumber"] =
                                            item.longName
                                        item.types.contains("country") -> resultsMap["county"] =
                                            item.longName
                                        item.types.contains("postal_code") -> resultsMap["zipcode"] =
                                            item.longName
                                        item.types.contains("locality") -> resultsMap["city"] =
                                            item.longName
                                        item.types.contains("route") -> resultsMap["street"] =
                                            item.longName
                                        item.types.contains("administrative_area_level_1") -> resultsMap["state"] =
                                            item.longName
                                    }
                                }
                                callback.doneResults(resultsMap)
                            }
                }
            }
        })
    }


    interface ApiRequest {
        @GET("place/{path}/json")
        fun getRequest(
            @Path("path") path: String?,
            @QueryMap parameters: MutableMap<String, String>
        ): retrofit2.Call<String>?
    }
}