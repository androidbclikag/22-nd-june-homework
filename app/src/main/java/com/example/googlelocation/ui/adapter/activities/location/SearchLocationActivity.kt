package com.example.googlelocation.ui.adapter.activities.location

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log.d
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.googlelocation.App
import com.example.googlelocation.R
import com.example.googlelocation.data.CustomCallBack
import com.example.googlelocation.data.FutureCallBack
import com.example.googlelocation.data.LocationLoader
import kotlinx.android.synthetic.main.city_text_field.*
import kotlinx.android.synthetic.main.search_location_activity.*
import kotlinx.android.synthetic.main.state_text_field.*
import kotlinx.android.synthetic.main.street_address_text_field.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.zip_text_field.*
import org.json.JSONObject

class SearchLocationActivity : AppCompatActivity(), View.OnClickListener {

    private val locItems = mutableListOf<LocationsModel>()
    private lateinit var locationsRecyclerViewAdapter: LocationsRecyclerViewAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.search_location_activity)
        init()
    }

    private fun init() {

        recyclerView.layoutManager = LinearLayoutManager(this)
        locationsRecyclerViewAdapter = LocationsRecyclerViewAdapter(locItems, itemClick)
        searchEditText.addTextChangedListener(textWatcher)
        recyclerView.adapter = locationsRecyclerViewAdapter
        attachBar()


    }

    private fun attachBar() {
        setSupportActionBar(toolBar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        doneButton.text = resources.getString(R.string.add)
        titleTV.text = resources.getString(R.string.area_of_interest)
        doneButton.setOnClickListener(this)


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            super.onBackPressed()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun getLocation(input: String) {
        val parameters: MutableMap<String, String> = mutableMapOf()
        val countryId = intent.extras!!.getString("countryID", "")
        parameters["input"] = input
        parameters["key"] = App.instance!!.getApiKey()
        parameters["language"] = "en"
        parameters["components"] = "country:$countryId"

        LocationLoader.getRequest(
            LocationLoader.AUTOCOMPLETE,
            parameters,
            object : CustomCallBack {
                override fun onError(throwable: String) {
                    super.onError(throwable)
                    Toast.makeText(
                        this@SearchLocationActivity,
                        "response failed",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onResponse(body: String) {
                    locItems.clear()
                    val json = JSONObject(body)
                    if (json.has("predictions")) {
                        val predictions = json.getJSONArray("predictions")
                        (0 until predictions.length()).forEach {
                            val prediction = predictions.getJSONObject(it)
                            locItems.add(
                                LocationsModel(
                                    prediction.getString("description"),
                                    prediction.getString("place_id")
                                )
                            )
                            recyclerView.adapter!!.notifyDataSetChanged()
                        }

                    }
                }
            })
    }


    private fun add() {

        if (searchEditText.text.isNotEmpty()) {
            val intent = intent
            intent.putExtra(
                "description",
                locItems[locationsRecyclerViewAdapter.selectedPosition].description
            )
            intent.putExtra(
                "place_id",
                locItems[locationsRecyclerViewAdapter.selectedPosition].place_id
            )
            setResult(Activity.RESULT_OK, intent)
            backToAddressActivity()
            finish()
        } else
            Toast.makeText(this, "Pleas choose an address", Toast.LENGTH_SHORT).show()


    }


    private val itemClick = object : CustomOnClick {
        override fun onClick() {
            searchEditText.setText(locItems[locationsRecyclerViewAdapter.selectedPosition].description)

        }


    }


    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            getLocation(s.toString())
        }


    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.doneButton -> add()

        }
    }


    private fun backToAddressActivity() {
        super.onBackPressed()
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out)

    }

    override fun onBackPressed() {
        backToAddressActivity()
    }


}
